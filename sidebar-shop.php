<div class="col-md-12">
    <?php
    global $post;
    $terms = wp_get_post_terms( $post->ID, 'product_cat' );
    foreach ( $terms as $term ) $categories[] = $term->slug;
    if ( in_array( '170ml', $categories ) ) {
        $args = array('post_type' => 'product', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date','tax_query' => array( array ( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => '170ml')));
    } else {
        $args = array('post_type' => 'product', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date','tax_query' => array( array ( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => 'nut-mix')));
    }
    ?>
    <?php query_posts($args); $url = 0; $i = 1; ?>
    <?php while (have_posts()) : the_post(); ?>
    <?php $color = get_post_meta(get_the_ID(), 'rw_circle', true); ?>
    <?php $darker = get_post_meta(get_the_ID(), 'rw_image', true); ?>
    <?php if ($darker == 0) { $url = get_template_directory_uri() . '/images/logo-product.png'; $darker_letter = "#FFFFFF"; } else { $url = get_template_directory_uri() . '/images/logo-invert.png'; $darker_letter = "#482912"; } ?>
    <div id="<?php echo get_the_ID(); ?>" class="sidebar-product-item" style="background-color: <?php echo $color; ?>;">
        <?php $title = get_the_title(); ?>
        <?php $divided_title = str_replace(' ', '<br />', $title); ?>
        <a href="<?php the_permalink(); ?>"><h5 class="sidebar-product-item-title" style="color:<?php echo $darker_letter; ?>;"><?php echo $divided_title; ?></h5></a>
    </div>
    <?php $i++; endwhile; ?>
    <?php wp_reset_query(); ?>
</div>
