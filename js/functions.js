var shareShow = false;
$(function () {
    "use strict";
    if ($("#contact-form").length > 0) {
        $("#contact-form").validationEngine('attach', {
            scroll: false,
            onValidationComplete: function (form, status) {
                if (status === true) {
                    $('#form-submit-wrapper').empty();
                    $('#form-submit-wrapper').addClass('loading');
                    var form_data = $('#contact-form').serialize();
                    $('#btn_submit').attr({disabled: 'disabled', value: 'ENVIANDO...'});
                    $.post('../contacto-submit', form_data, function (data) {
                        $('#form-submit-wrapper').html(data);
                        $('#form-submit-wrapper').removeClass('loading');
                        $('#btn_submit').attr({disabled: 'disabled', value: 'LISTO!'});
                    });
                }
            }
        });
    }
});

$(document).ready(function () {
    "use strict";
    $("#sticker").sticky({topSpacing: 0});
    $("#search").validationEngine();
    $("body").niceScroll({
        cursorcolor: '#BE1B20',
        cursorborder: '0px',
        cursorwidth: '10px',
        cursorborderradius: '0px',
        background: '#000000',
        hwacceleration: false,
        scrollspeed: 60,
        mousescrollstep: 85,
        smoothscroll: true,
        autohidemode: false,
        zindex: '999'
    });
    $(".about-section-title").lettering('words');
    $(".special-page-title").lettering('words');
    $(".contact-title").lettering('lines');
    $(".blog-title").lettering('lines');
    $(".cart_totals h2").lettering('words');
    $("#order_review_heading").lettering('words');
    $(".widget h2").lettering('words');
    $('.section-title').lettering('words');

    $(".nuxbox-hero-container").owlCarousel({
        autoPlay : 4000,
        navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        pagination: false,
        singleItem:true,
        navigationText: ["<span class='sprite-slider sprite-slider-prev'></span>","<span class='sprite-slider sprite-slider-next'></span>"]

        // "singleItem:true" is a shortcut for:
        // items : 1,
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false

    });

    $(".related-products").owlCarousel({
        items: 4,
        pagination: false,
        navigation: true,
        navigationText: ["<span class='sprite-slider sprite-slider-prev'></span>","<span class='sprite-slider sprite-slider-next'></span>"]
    });
    var $grid = $('.archive-container').masonry({
        // options
        itemSelector: '.archive-item',
        gutter: 15
    });
    // layout Masonry after each image loads
    $grid.imagesLoaded().progress( function() {
        $grid.masonry('layout');
    });
});

$('.nuxbox-creator-item-1').click( function() {
    $('.nuxbox-creator-item-1').addClass('fadeOutLeft');
});
$('.nuxbox-creator-item-2').click( function() {
    $('.nuxbox-creator-item-2').addClass('fadeOut');
});
$('.nuxbox-creator-item-3').click( function() {
    $('.nuxbox-creator-item-3').addClass('fadeOutRight');
});

$('.component_option_thumbnail_tap').click( function() {

    $('.composite_navigation .next').addClass('vibrate-heart');

});
$('.next').click( function() {
    $(this).removeClass('vibrate-heart');
    $('.arrow-single').removeClass('hidden-arrow');
    $('.arrow-single').addClass('show-arrow');

});

$('.summary_element_tap').click( function() {
    $('.arrow-single').removeClass('show-arrow');
    $('.arrow-single').addClass('hidden-arrow');
});


function frontchanger(x){
    "use strict";
    $('.front-product-item').removeClass('front-product-item-active');
    $('#' + x).addClass('front-product-item-active');
    $.ajax({
        type: 'POST',
        url: ruta_blog + '/inc/front-function.php',
        data: {
            datos: x
        },
        beforeSend: function () {
            $('#front-product-wrapper').html(
                '<div class="loader">' +
                '<div id="loader-wrapper"><div id="loader"></div></div>' +
                '</div>'
            );
        },
        success: function (resp) {
            setTimeout(function () {
                $('#front-product-wrapper').html(resp);
            }, 1000);
        },
        error: function () {
            alert('error');
        }
    });
    return false;
}

$('#share-btn').click(function (){
    if (shareShow == false) {
        $('#special-share').addClass('show-share');
        $('#special-share').removeClass('hide-share');
        shareShow = true;
    } else {
        $('#special-share').removeClass('show-share');
        $('#special-share').addClass('hide-share');
        shareShow = false;
    }
});


function nuxboxadd(x){
    alert(x);
}

smoothScroll.init({
    selector: '[data-scroll]', // Selector for links (must be a valid CSS selector)
    selectorHeader: '[data-scroll-header]', // Selector for fixed headers (must be a valid CSS selector)
    speed: 1500, // Integer. How fast to complete the scroll in milliseconds
    easing: 'easeInOutCubic', // Easing pattern to use
    updateURL: true, // Boolean. Whether or not to update the URL with the anchor hash on scroll
    offset: 0, // Integer. How far to offset the scrolling anchor location in pixels
    callback: function ( toggle, anchor ) {} // Function to run after scrolling
});

var wow = new WOW(
    {
        boxClass:     'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset:       400,          // distance to the element when triggering the animation (default is 0)
        mobile:       true,       // trigger animations on mobile devices (default is true)
        live:         true,       // act on asynchronously loaded content (default is true)
        callback:     function(box) {
            // the callback is fired every time an animation is started
            // the argument that is passed in is the DOM node being animated
        },
        scrollContainer: null // optional scroll container selector, otherwise use window
    }
);
wow.init();
