<?php get_header('shop'); ?>
<?php the_post(); ?>
<main class="container" role="main">
    <div class="row">
        <section class="nuxbox-creator-container col-md-12">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h1 class="panel-title">
                            <a>
                                <span></span> <span class="nuxbox-creator-title">select <strong>the box</strong></span>
                            </a>
                        </h1>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <?php $args = array('post_type' => 'product', 'order'=> 'ASC', 'orderby' => 'date', 'posts_per_page' => 3, 'tax_query' => array( array( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => 'healthy-box'))); ?>
                                <?php query_posts($args); $i = 1; ?>
                                <div class="col-md-10 col-md-offset-1">
                                    <?php while (have_posts()) : the_post(); ?>
                                    <div class="nuxbox-creator-item nuxbox-creator-item-<?php echo $i; ?> col-md-6 delay-1 animated">
                                        <h2><?php the_title(); ?></h2>
                                        <h4>4 spreads</h4>
                                        <div class="clearfix"></div>
                                        <a href="<?php the_permalink(); ?>">
                                            <div class="col-md-10">
                                                <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
                                            </div>
                                        </a>
                                    </div>
                                    <?php $i++; endwhile; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
