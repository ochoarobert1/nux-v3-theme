<?php get_header(); ?>
<?php the_post(); ?>
<main class="container" role="main">
    <div class="row">
        <section class="col-md-9 col-sm-12 col-xs-12 no-paddingl <?php echo join(' ', get_post_class()); ?>">
            <h1 class="section-title"><?php the_title(); ?></h1>
            <article id="post-<?php the_ID(); ?>" >
                <?php the_content(); ?>
            </article>
        </section>
        <aside class="the-sidebar the-sidebar-faq col-md-3 no-paddingr hidden-xs hidden-sm" role="complementary">
            <?php get_sidebar(); ?>
        </aside>
    </div>
</main>
<?php get_footer(); ?>
