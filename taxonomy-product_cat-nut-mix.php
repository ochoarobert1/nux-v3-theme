<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author         WooThemes
 * @package     WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<?php
/**
         * woocommerce_before_main_content hook
         *
         * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
         * @hooked woocommerce_breadcrumb - 20
         */
do_action( 'woocommerce_before_main_content' );
?>

<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

<h1 class="page-title special-page-title text-center">pick your favourite mix!</h1>

<?php endif; ?>

<?php
/**
             * woocommerce_archive_description hook
             *
             * @hooked woocommerce_taxonomy_archive_description - 10
             * @hooked woocommerce_product_archive_description - 10
             */
do_action( 'woocommerce_archive_description' );
?>

<?php $args = array('post_type' => 'product','paged' => $paged, 'posts_per_page' => 8, 'order' => 'ASC', 'orderby' => 'date', 'tax_query' => array( array ( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => 'nut-mix' ))); ?>

<?php query_posts($args); ?>

<?php if ( have_posts() ) : ?>

<?php
/**
                 * woocommerce_before_shop_loop hook
                 *
                 * @hooked woocommerce_result_count - 20
                 * @hooked woocommerce_catalog_ordering - 30
                 */
do_action( 'woocommerce_before_shop_loop' );
?>

<?php woocommerce_product_loop_start(); ?>

<?php woocommerce_product_subcategories(); ?>
<div class="col-md-12">
    <?php while ( have_posts() ) : the_post(); ?>
    <?php wc_get_template_part( 'content', 'product' ); ?>
    <?php endwhile; // end of the loop. ?>
</div>
<?php woocommerce_product_loop_end(); ?>

<?php
/**
                 * woocommerce_after_shop_loop hook
                 *
                 * @hooked woocommerce_pagination - 10
                 */
do_action( 'woocommerce_after_shop_loop' );
?>

<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

<?php wc_get_template( 'loop/no-products-found.php' ); ?>

<?php endif; ?>

<?php
/**
         * woocommerce_after_main_content hook
         *
         * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
         */
do_action( 'woocommerce_after_main_content' );
?>

<?php
/**
         * woocommerce_sidebar hook
         *
         * @hooked woocommerce_get_sidebar - 10

        do_action( 'woocommerce_sidebar' );*/
?>

<section class="nuxbox-hero-prods col-md-12 no-paddingl no-paddingr">
    <div class="container">
        <div class="row">
           <div class="nuxbox-hero-mask col-xs-12 hidden-sm hidden-md hidden-lg"></div>
            <div class="col-md-4 hidden-xs col-sm-4">
            </div>
            <div class="nuxbox-container2 col-md-4 col-sm-4 col-xs-12">
                <h4 class="nuxbox-hero-text">customize your</h4>
                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-nuxbox.png" alt="nuxbox logo" class="img-responsive" />
                <h2 class="nuxbox-hero-text">choose smart!</h2>
                <div class="clearfix"></div>
            </div>
            <div class="nuxbox-hero-prods-btn-container col-md-3 col-sm-3 col-xs-12">
                <a href="<?php echo home_url('/nuxbox-creator'); ?>"><button class="btn btn-nuxbox-hero2">buy box</button></a>
            </div>
        </div>
    </div>
</section>


<?php get_footer( 'shop' ); ?>
