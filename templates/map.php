
<script type="text/javascript">
    var map = null;
    var marker = null;
    var dataMarkers = [];
    var infowindow = '';

    /* LLAMO A LAS UBICACIONES */

    <?php $args = array('post_type' => 'partners', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'name'); ?>
    <?php query_posts($args); $i = 1; ?>
    <?php while (have_posts()) : the_post(); ?>
    <?php $link = get_post_meta(get_the_ID(), 'rw_partner-link', true); ?>

    dataMarkers[<?php echo $i; ?>] = "<?php echo get_the_title(); ?>|<?php echo get_post_meta(get_the_ID(), 'rw_partner-loc', true); ?>|<?php echo get_post_meta(get_the_ID(), 'address', true); ?>|<?php echo $link; ?>";

    <?php $i++; endwhile; ?>

</script>

<?php $coordinates = '49.611485, 6.127688'; ?>

<script type="text/javascript" src="//maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript">



    $(document).ready(function() {
        var mapOptions = {
            center: new google.maps.LatLng(<?php echo $coordinates ?>),
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
            navigationControl: false,
            scaleControl: false,
            optimized: false,
            styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}],
            zIndex:0
            /* HYBRID | ROADMAP | SATELLITE| TERRAIN */
        };

        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

        var i = 1;
        var res = null;
        console.log(dataMarkers.length);
        for (i = 1; i < dataMarkers.length; i++) {

            res = dataMarkers[i].split("|");
            res2 = res[1].split(",");
//            console.log(res2);

            var myHome = { "lat" : res2[0] , "long" : res2[1] };
            /* CREO EL MARKER */
            marker = new google.maps.Marker({
                position: new google.maps.LatLng( myHome.lat, myHome.long),
                title: res[0],
                animation: google.maps.Animation.DROP,
                map: map
                //icon: "<?php bloginfo('template_url') ?>/images/marker.png"
            });
            /* ATTACH DATA WITH THE MARKER INFO WITH A FUNCTION */
            attachdataMarker(marker, i);

            /* FUNCTION TO ATTACH DATA */
            function attachdataMarker(marker, numb) {
                /* SPLIT MY ACTUAL ARRAY INTO THE TWO VALUES */
                var res = dataMarkers[numb].split("|");
                /* FILLS infoWindow WITH DATA */
                var infowindow = new google.maps.InfoWindow({
                    content: "<div id='iw-container'><div class='col-md-12 iw-title'><h1>" + res[0] + "</h1></div><div class='iw-content-marker col-md-12 text-center'><p>Address: " + res[2] + "</p><p>URL: <a href='"+ res[3] +"' target='_blank'>"+ res[3] +"</a></p></div></div>"
                });
                /* ADDS A LISTENER WHEN CLICK A MARKER */
                marker.addListener('click', function() {
                    infowindow.open(marker.get('map'), marker);
                });
            }
        }

    });
</script>
<div class="clearfix"></div>
<div class="map">
    <div id="map_canvas" class="map_gray" style="width:100%; height:335px;"></div>
</div>
<div class="clearfix"></div>
