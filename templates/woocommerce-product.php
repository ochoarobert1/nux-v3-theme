<a href="<?php the_permalink(); ?>">

    <div class="shop-product-item col-md-6 no-paddingl no-paddingr">
        <div class="shop-product-item-img col-md-12 no-paddingl no-paddingr">
            <?php the_post_thumbnail('product_front_single', $defaultatts)?>
        </div>
        <div class="shop-product-item-button col-md-12">
            <a href="<?php the_permalink(); ?>"><span>CHECK</span> <span class="btn">></span></a>
        </div>
    </div>

</a>
