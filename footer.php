</div>
</div>
<footer class="container-fluid" role="contentinfo">
    <div class="row">
        <div class="the-footer col-md-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div id="partners" class="footer-item col-md-4 col-sm-4 col-xs-12">
                        <h3 class="text-center">Our history</h3>
                        <?php $footerID = get_page_by_path('home'); ?>
                        <p><?php echo get_post_meta($footerID->ID, 'rw_footer-text', true); ?></p>
                        <div class="footer-symbol col-md-3 col-xs-3 col-sm-3">
                            <span class="sprites-footer sprites-about-chat"></span>
                        </div>
                        <div class="footer-partners col-md-9 col-xs-9 col-sm-9">
                        <?php echo get_post_meta($footerID->ID, 'rw_aliances', true); ?>
<!--
                            <h4>available in</h4>
                            <a href="http://knopes.com" target="_blank"><p>Knopes</p></a>
                            <h4>pickup points</h4>
                            <a href="http://amapura.lu" target="_blank"><p>Amapura</p></a>
-->
                        </div>

                    </div>
                    <div class="footer-item col-md-4 col-sm-4 col-xs-6 hidden-xs">
                        <h3>Recent posts</h3>
                        <ul>
                            <?php $args = array('post_type' => 'post', 'posts_per_page' => 8, 'order' => 'DESC', 'orderby' => 'date'); ?>
                            <?php query_posts($args); ?>
                            <?php while (have_posts()) : the_post(); ?>
                            <li><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></li>
                            <?php endwhile; ?>
                            <?php wp_reset_query(); ?>
                        </ul>
                    </div>
                    <div class="footer-item col-md-4 col-sm-4 col-xs-6 hidden-xs">
                        <h3>Our spreads!</h3>
                        <ul>
                            <?php $args = array('post_type' => 'product', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date', 'tax_query' => array( array ( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => '170ml' ))); ?>
                            <?php query_posts($args); ?>
                            <?php while (have_posts()) : the_post(); ?>
                            <li><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></li>
                            <?php endwhile; ?>
                            <?php wp_reset_query(); ?>
                        </ul>
                    </div>
                    <!--HIDDEN XS -->
                    <div class="footer-item col-xs-12 hidden-sm hidden-md hidden-lg">
                        <h3>our spreads!</h3>
                        <ul>
                            <?php $args = array('post_type' => 'product', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date', 'tax_query' => array( array ( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => '170ml' ))); ?>
                            <?php query_posts($args); ?>
                            <?php while (have_posts()) : the_post(); ?>
                            <li><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></li>
                            <?php endwhile; ?>
                            <?php wp_reset_query(); ?>
                        </ul>
                    </div>
                    <div class="footer-item col-xs-12 hidden-sm hidden-md hidden-lg">
                        <h3>recent posts</h3>
                        <ul>
                            <?php $args = array('post_type' => 'post', 'posts_per_page' => 8, 'order' => 'ASC', 'orderby' => 'date'); ?>
                            <?php query_posts($args); ?>
                            <?php while (have_posts()) : the_post(); ?>
                            <li><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></li>
                            <?php endwhile; ?>
                            <?php wp_reset_query(); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copy col-md-12 no-paddingl no.paddingr text-center">
            <div class="col-md-8">
                <p><span>Nux &copy; 2015</span> | <span>All Rights Reserved</span> <span>e-mail: <a href="mailto:hello@nuxorganicfoods.com">hello@nuxorganicfoods.com</a></span></p>
            </div>
            <div class="dlk-container col-md-4">
                DEVELOPED BY <span><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/dlk.png" alt="" class="img-responsive"></span>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>
</body>
</html>


