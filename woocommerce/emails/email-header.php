<?php
/**
 * Email Header
 *
 * @author  WooThemes
 * @package WooCommerce/Templates/Emails
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

?>
<!DOCTYPE html>
<html dir="<?php echo is_rtl() ? 'rtl' : 'ltr'?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title><?php echo get_bloginfo( 'name', 'display' ); ?></title>
    </head>
    <body <?php echo is_rtl() ? 'rightmargin' : 'leftmargin'; ?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
        <div id="wrapper" dir="<?php echo is_rtl() ? 'rtl' : 'ltr'?>">
            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                <tr>
                    <td align="center" valign="top">
                        <div id="template_header_image">
                            <?php
    if ( $img = get_option( 'woocommerce_email_header_image' ) ) {
        echo '<p style="margin-top:0;"><img src="' . esc_url( $img ) . '" alt="' . get_bloginfo( 'name', 'display' ) . '" /></p>';
    }
                            ?>
                        </div>
                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container">
                            <tr>
                                <td align="center" valign="top">
                                    <!-- Header -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_header">
                                        <tr>
                                            <td id="header_wrapper">
                                                <table>
                                                    <tr>
                                                        <td width="200"><img src="http://www.nuxorganicfoods.com/wp-content/uploads/2016/01/logo-sl.png" alt="nuxorganicfoods" width="150" height="76" /></td>
                                                        <td width="400">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="400">
                                                                <tr>
                                                                    <td align="right">
                                                                        <a href="https://www.facebook.com/nuxorganicfoods" target="_blank" title="nuxorganicfoods on facebook"><img src="http://www.nuxorganicfoods.com/wp-content/uploads/2016/01/fb.png" alt="Facebook" width="25" height="25" /></a>
                                                                    </td>
                                                                    <td align="right" width="30">
                                                                        <a href="https://www.instagram.com/nuxorganicfoods" target="_blank" title="nuxorganicfoods on instagram"><img src="http://www.nuxorganicfoods.com/wp-content/uploads/2016/01/in.png" alt="instagram" width="25" height="25"/></a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <h1><?php echo $email_heading; ?></h1>
                                                        </td>
                                                    </tr>
                                                </table>


                                            </td>
                                        </tr>
                                    </table>
                                    <!-- End Header -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- Body -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body">
                                        <tr>
                                            <td valign="top" id="body_content">
                                                <!-- Content -->
                                                <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td valign="top">
                                                            <div id="body_content_inner">
