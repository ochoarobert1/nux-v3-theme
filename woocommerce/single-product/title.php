<?php
/**
 * Single Product title
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

?>
<h1 itemprop="name" class="product_title entry-title"><?php the_title(); ?></h1>
<?php global $product; ?>
<?php if( $product->is_type( 'simple' ) ){
    $terms = get_the_terms( get_the_ID(), 'product_cat' );

    if ( $terms && ! is_wp_error( $terms ) ) {
        $draught_links = array();
        foreach ( $terms as $term ) {
            if ($term->name != 'singles') {
                $draught_links[] = $term->name;
            }
        }
        $on_draught = join( ", ", $draught_links );
    }
?>
<p class="category-single-float"><span><?php echo $on_draught; ?></span></p>
<?php } ?>
