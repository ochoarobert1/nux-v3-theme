<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.9
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

global $product;

?>
<?php $composite_selector = get_post_meta(get_the_ID(), 'rw_composite', true); ?>
<?php if ($composite_selector != 1) { ?>
<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">

    <p class="price"><?php echo $product->get_price_html(); ?></p>

    <meta itemprop="price" content="<?php echo esc_attr( $product->get_price() ); ?>" />
    <meta itemprop="priceCurrency" content="<?php echo esc_attr( get_woocommerce_currency() ); ?>" />
    <?php
                                     // Availability
                                     $availability      = $product->get_availability();
                                     $availability_html = empty( $availability['availability'] ) ? '' : '<p class="stock ' . esc_attr( $availability['class'] ) . '"><span>Availability: </span>' . esc_html( $availability['availability'] ) . '</p>';

                                     echo apply_filters( 'woocommerce_stock_html', $availability_html, $availability['availability'], $product );
    ?>

</div>
<?php } ?>
