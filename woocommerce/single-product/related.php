<?php
/**
 * Related Products
 *
 * @author         WooThemes
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

if ( empty( $product ) || ! $product->exists() ) {
    return;
}

$related = $product->get_related( 8 );

if ( sizeof( $related ) == 0 ) return;

global $post;
$terms = wp_get_post_terms( $post->ID, 'product_cat' );
foreach ( $terms as $term ) $categories[] = $term->slug;
if ( in_array( '170ml', $categories ) ) {
    $args = array('post_type' => 'product', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date','tax_query' => array( array ( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => '170ml')));
} else {
    $args = array('post_type' => 'product', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date','tax_query' => array( array ( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => 'nut-mix')));
}

$products = new WP_Query( $args );

$woocommerce_loop['columns'] = $columns;

if ( $products->have_posts() ) : ?>

<div class="related products">

    <h2><?php _e( 'Other Spreads!', 'nux' ); ?></h2>



    <?php woocommerce_product_loop_start(); ?>
    <div class="related-products">
        <?php while ( $products->have_posts() ) : $products->the_post(); ?>
        <div class="related-products-item">
            <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail(); ?>
            </a>
            <a href="<?php the_permalink(); ?>">
                <div class="related-products-item-btn col-md-12">
                    <span>CHECK</span> <span class="btn">&gt;</span>
                </div>
            </a>
        </div>
        <?php /* wc_get_template_part( 'content', 'product' ); */?>

        <?php endwhile; // end of the loop. ?>
    </div>
    <?php woocommerce_product_loop_end(); ?>

</div>

<?php endif;

wp_reset_postdata();
