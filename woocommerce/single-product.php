<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author         WooThemes
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>



<?php
/**
         * woocommerce_before_main_content hook
         *
         * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
         * @hooked woocommerce_breadcrumb - 20
         */
do_action( 'woocommerce_before_main_content' );
?>

<?php if( $product->is_type( 'composite' ) ){ ?>
</div>
</div>

<!--
<div class="nuxbox-composite-item col-md-12">
<h1 class="panel-title">
<span class="nuxbox-creator-title-2">add your <br /><strong>favourites spreads</strong></span>
</h1>
</div>
-->

<div class="col-md-12">
    <div class="sidebar-single-prods col-md-2">
        <?php /* $args2 = array('post_type' => 'product', 'posts_per_page' => 8, 'order' => 'ASC', 'orderby' => 'name', 'tax_query' => array(array('taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => '90g',),),); ?>
        <?php query_posts( $args2 ); ?>
        <?php $i = 1; if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php $arreglo[$i] = get_the_ID(); ?>
        <?php $i++; endwhile; endif; ?>
        <?php for ($i = 1; $i <= 4; $i++) { ?>
        <div class="sidebar-single-prods-item col-md-12">
            <?php $post_sidebar = get_post($arreglo[$i]); ?>
            <div class="col-md-9 no-paddingl no-paddingr">
                <?php echo get_the_post_thumbnail( $post_sidebar->ID, 'thumbnail' ); ?>
            </div>
            <div class="sidebar-single-prods-item-ingr col-md-3 no-paddingl">
                <?php $spreading = get_post_meta( $post_sidebar->ID, 'rw_ingredients', true); ?>
                <?php echo $spreading; ?>
            </div>
        </div>
        <?php } ?>
        <?php wp_reset_postdata(); ?>
        <?php wp_reset_query();  */ ?>
    </div>
    <div class="composite-single-main-component col-md-8">
        <div class="arrow-single arrow1 show-arrow"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/arrow-left1.png" alt=""></div>
        <div class="arrow-single arrow2 show-arrow"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/arrow-left2.png" alt=""></div>
        <div class="arrow-single arrow3 show-arrow"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/arrow-right1.png" alt=""></div>
        <div class="arrow-single arrow4 show-arrow"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/arrow-right2.png" alt=""></div>


        <?php while ( have_posts() ) : the_post(); ?>
        <?php wc_get_template_part( 'content', 'single-product' ); ?>
        <?php endwhile; // end of the loop. ?>
    </div>
    <div class="sidebar-single-prods col-md-2">
        <?php /* $args2 = array('post_type' => 'product', 'posts_per_page' => 8, 'order' => 'ASC', 'orderby' => 'name', 'tax_query' => array(array('taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => '90g',),),); ?>
        <?php query_posts( $args2 ); ?>
        <?php $i = 1; if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php $arreglo[$i] = get_the_ID(); ?>
        <?php $i++; endwhile; endif; ?>
        <?php for ($i = 5; $i <= 8; $i++) { ?>
        <div class="sidebar-single-prods-item col-md-12">
            <?php $post_sidebar = get_post($arreglo[$i]); ?>
            <div class="sidebar-single-prods-item-ingr col-md-3 no-paddingr">
                <?php $spreading = get_post_meta( $post_sidebar->ID, 'rw_ingredients', true); ?>
                <?php echo $spreading; ?>
            </div>
            <div class="col-md-9 no-paddingl no-paddingr">
                <?php echo get_the_post_thumbnail( $post_sidebar->ID, 'thumbnail' ); ?>
            </div>
        </div>
        <?php } ?>
        <?php wp_reset_postdata(); ?>
        <?php wp_reset_query(); */ ?>
    </div>
</div>
<?php } ?>

<?php if( $product->is_type( 'simple' ) ){ ?>

<div class="single-sidebar-container col-md-2 col-xs-12 col-sm-12">
    <?php
    /**
         * woocommerce_sidebar hook
         *
         * @hooked woocommerce_get_sidebar - 10
         */
    do_action( 'woocommerce_sidebar' );
    ?>
</div>
<div class="col-md-9 col-md-offset-1 col-sm-12 col-xs-12">
    <?php while ( have_posts() ) : the_post(); ?>

    <?php wc_get_template_part( 'content', 'single-product' ); ?>

    <?php endwhile; // end of the loop. ?>

    <?php
    /**
         * woocommerce_after_main_content hook
         *
         * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
         */
    do_action( 'woocommerce_after_main_content' );
    ?>
</div>

<?php } ?>

<?php if( $product->is_type( 'bundle' ) ){ ?>

<div class="col-md-12">
    <div class="sidebar-single-prods col-md-2">
        <?php /* $args2 = array('post_type' => 'product', 'posts_per_page' => 8, 'order' => 'ASC', 'orderby' => 'name', 'tax_query' => array(array('taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => '90g',),),); ?>
        <?php query_posts( $args2 ); ?>
        <?php $i = 1; if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php $arreglo[$i] = get_the_ID(); ?>
        <?php $i++; endwhile; endif; ?>
        <?php for ($i = 1; $i <= 4; $i++) { ?>
        <div class="sidebar-single-prods-item col-md-12">
            <?php $post_sidebar = get_post($arreglo[$i]); ?>
            <div class="col-md-9 no-paddingl no-paddingr">
                <?php echo get_the_post_thumbnail( $post_sidebar->ID, 'thumbnail' ); ?>
            </div>
            <div class="sidebar-single-prods-item-ingr col-md-3 no-paddingl">
                <?php $spreading = get_post_meta( $post_sidebar->ID, 'rw_ingredients', true); ?>
                <?php echo $spreading; ?>
            </div>
        </div>
        <?php } ?>
        <?php wp_reset_postdata(); ?>
        <?php wp_reset_query();  */ ?>
    </div>
    <div class="composite-single-main-bundle col-md-8">
        <?php while ( have_posts() ) : the_post(); ?>
        <?php wc_get_template_part( 'content', 'single-product' ); ?>
        <?php endwhile; // end of the loop. ?>
    </div>
    <div class="sidebar-single-prods col-md-2">
        <?php /* $args2 = array('post_type' => 'product', 'posts_per_page' => 8, 'order' => 'ASC', 'orderby' => 'name', 'tax_query' => array(array('taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => '90g',),),); ?>
        <?php query_posts( $args2 ); ?>
        <?php $i = 1; if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php $arreglo[$i] = get_the_ID(); ?>
        <?php $i++; endwhile; endif; ?>
        <?php for ($i = 5; $i <= 8; $i++) { ?>
        <div class="sidebar-single-prods-item col-md-12">
            <?php $post_sidebar = get_post($arreglo[$i]); ?>
            <div class="sidebar-single-prods-item-ingr col-md-3 no-paddingr">
                <?php $spreading = get_post_meta( $post_sidebar->ID, 'rw_ingredients', true); ?>
                <?php echo $spreading; ?>
            </div>
            <div class="col-md-9 no-paddingl no-paddingr">
                <?php echo get_the_post_thumbnail( $post_sidebar->ID, 'thumbnail' ); ?>
            </div>
        </div>
        <?php } ?>
        <?php wp_reset_postdata(); ?>
        <?php wp_reset_query(); */ ?>
    </div>
</div>

<?php } ?>

<?php get_footer( 'shop' ); ?>
