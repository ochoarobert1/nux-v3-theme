<?php get_header(); ?>
<main class="container" role="main">
    <div class="row">
        <section class="col-md-12">
            <div class="col-md-9 no-paddingl">
                <h3 class="blog-title">The unique variety of Nux is all about organic and nutritious ingredients <br/> packed with antioxidants, anti-inflammatory vitamins and minerals.</h3>
            </div>
            <div class="clearfix"></div>
            <hr>
            <div class="col-md-9 no-paddingl">
                <div class="archive-container">
                    <?php $defaultatts = array('class' => 'img-responsive'); ?>
                    <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                    <article id="post-<?php the_ID(); ?>" class="archive-item wow animated fadeInUp no-paddingl no-paddingr <?php echo join(' ', get_post_class()); ?>">
                        <picture class="col-md-12 no-paddingl no-paddingr">
                            <?php if ( has_post_thumbnail()) : ?>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                <?php the_post_thumbnail('blog_img', $defaultatts); ?>
                            </a>
                            <?php else : ?>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/no-img.jpg" alt="No img" class="img-responsive" />
                            </a>
                            <?php endif; ?>
                        </picture>
                        <div class="col-md-12 no-paddingl no-paddingr">
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h2><?php the_title(); ?></h2></a>
                            <p><?php the_excerpt(); ?></p>
                            <?php edit_post_link(); ?>
                            <div class="plus"><a href="<?php the_permalink(); ?>">Read More</a></div>
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                    </article>
                    <?php endwhile; ?>
                </div>
                <div class="clearfix"></div>
                <div class="pagination col-md-12">
                    <?php if(function_exists('wp_paginate')) { wp_paginate(); } else { posts_nav_link(); } ?>
                </div>
            </div>
            <div class="col-md-3 no-paddingr hidden-xs hidden-sm">
                <?php get_sidebar(); ?>
            </div>
            <?php else: ?>
            <article>
                <h2>Disculpe, su busqueda no arrojo ningun resultado</h2>
                <h3>Haga click <a href="<?php echo home_url('/'); ?>">aqui</a> para volver al inicio</h3>
            </article>
            <?php endif; ?>
        </section>
    </div>
</main>
<?php get_footer(); ?>
