<?php get_header(); ?>
<?php the_post(); ?>
<main class="container" role="main">
    <div class="row">
        <section class="col-md-12 <?php echo join(' ', get_post_class()); ?>">
            <h1 class="section-title"><?php the_title(); ?></h1>
            <article id="post-<?php the_ID(); ?>" >
                <?php the_content(); ?>
            </article>
        </section>
    </div>
</main>
<?php get_footer(); ?>
