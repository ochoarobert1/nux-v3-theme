<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main">
    <div class="row">
        <section class="the-slider col-md-12 col-sm-12 col-xs-12">
            <?php the_content(); ?>
        </section>
        <section class="the-about col-md-12 col-sm-12 col-xs-12">
            <div class="container">
                <div class="row">

                    <div class="about-item col-md-4 col-sm-4 col-xs-12">
                        <a data-scroll href="#partners">
                            <span class="sprites-about sprites-about-chat"></span>
                            <h3>where to find us?</h3>
                            <p>choose from a variety of spreads at our partners' stores</p>
                        </a>
                    </div>
                    <div class="about-item col-md-4 col-sm-4 col-xs-12">
                        <a href="<?php echo home_url('/blog'); ?>">
                            <span class="sprites-about sprites-about-recipe"></span>
                            <h3>our recipes</h3>
                            <p>curious about how to make Nux part of your daily diet?</p>
                        </a>
                    </div>
                    <div class="about-item col-md-4 col-sm-4 col-xs-12">
                        <a href="<?php echo home_url('/contact'); ?>">
                            <span class="sprites-about sprites-about-contact"></span>
                            <h3>contact us</h3>
                            <p>feel free to ask questions, make suggestions or just say hello!</p>

                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <h2 class="about-section-title">nuts about nuts</h2>         <p class="about-section-subtitle">indulge yourself with our spectacular nutty products!</p>
                </div>

            </div>
        </section>
        <section class="front-products col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="front-product-container col-md-6 col-sm-6 col-xs-6">
                        <?php $args = array('post_type' => 'product', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date','tax_query' => array( array ( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => '170ml' ))); ?>
                        <?php query_posts($args); $url = 0; $i = 1; $y = 1; ?>
                        <?php while (have_posts()) : the_post(); ?>

                        <?php $color = get_post_meta(get_the_ID(), 'rw_circle', true); ?>
                        <?php $darker = get_post_meta(get_the_ID(), 'rw_image', true); ?>
                        <?php if ($darker == 0) { $url = esc_url(get_template_directory_uri()) . '/images/logo-product.png'; } else { $url = esc_url(get_template_directory_uri()) . '/images/logo-invert.png'; }?>
                        <div id="<?php echo get_the_ID(); ?>" onclick="frontchanger(this.id)" class="front-product-item <?php if ($i === 1) { echo 'front-product-item-active'; }?> <?php if ($y == 2) { echo 'front-product-item-lateral'; } ?>" style="background-color: <?php echo $color; ?>; background-image: url(<?php echo $url; ?>)">
                            <h5 class="front-product-item-title" <?php if ($darker == 1) { echo 'style="color: #701A04;"'; } ?>><?php the_title(); ?></h5>
                        </div>
                        <?php $i++; $y++; if ($y > 2) { $y = 1; } endwhile; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                    <div class="col-md-5 col-md-offset-1 col-sm-6 col-xs-6">
                        <div id="front-product-wrapper" class="front-product-single col-md-12">
                            <?php $args = array('post_type' => 'product', 'posts_per_page' => 1, 'order' => 'ASC', 'orderby' => 'date','tax_query' => array( array ( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => '170ml' ))); ?>
                            <?php query_posts($args); $url = 0; ?>
                            <?php while (have_posts()) : the_post(); ?>
                            <?php $image = rwmb_meta( 'rw_title_img', 'type=image&size=front_letters', get_the_ID() ); ?>
                            <?php foreach ($image as $img) { $url = $img['full_url']; } ?>
                            <img src="<?php echo $url; ?>" alt="<?php echo get_the_title(); ?>" class="front-product-title img-responsive" />
                            <div class="front-product-single-img col-md-12">
                                <?php the_post_thumbnail('front_products'); ?>                                                       </div>
                            <a href="<?php the_permalink(); ?>"><button class="front-product-single-btn">buy</button></a>
                            <?php endwhile; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                        <div class="front-product-single-info col-md-12 col-sm-12 col-xs-12">
                            <h2>with Nux <strong>you can</strong></h2>
                            <p>energize your breakfast</p>
                            <p>make tasty snacks</p>
                            <p>fuel your workouts</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="nuxbox-hero-container col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="nuxbox-hero col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                <div class="container">
                    <div class="row">
                        <div class="nuxbox-hero-mask col-xs-12 hidden-sm hidden-md hidden-lg"></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        </div>
                        <div class="nuxbox-container col-md-5 col-md-offset-1 col-sm-6 col-xs-12">
                            <h4 class="nuxbox-hero-text">customize your</h4>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-nuxbox.png" alt="nuxbox logo" class="img-responsive" />
                            <h2 class="nuxbox-hero-text">choose smart!</h2>
                            <p>to find out how to make Nux</p>
                            <p>part of your daily diet, follow us</p>
                            <p>on Facebook and Instagram</p>
                            <div class="clearfix"></div>
                            <div class="nuxbox-buttons col-md-12 col-sm-12 col-xs-12">
                                <div class="nuxbox-social col-md-6 col-sm-6 col-xs-6">
                                    <a href="https://www.facebook.com/nuxorganicfoods" target="_blank"><i class="fa fa-facebook fa-3x"></i></a>
                                    <a href="https://www.instagram.com/nuxorganicfoods/" target="_blank"><i class="fa fa-instagram fa-3x"></i></a>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <a href="<?php echo home_url('/nuxbox-creator')?>"><button class="btn btn-nuxbox-hero">buy box</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="nuxbox-hero nutmix-hero col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                <div class="container">
                    <div class="row">
                        <div class="nutmix-hero-container col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                            <div class="nutmix-hero-title col-md-12 col-sm-12 col-xs-12">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/nutmix.png" alt="Nutmix"> <span>the energy kick!</span>
                            </div>
                            <div class="nutmix-hero-content col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/nut_mix_prods2.png" alt="Nutmix"  class="img-responsive"/>
                            </div>
                            <div class="nutmix-hero-footer col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-8 col-xs-8 no-paddingr">
                                    <p>to find out how to make Nux part of your daily diet, </p><p>follow us on Facebook and Instagram</p>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 no-paddingl">
                                    <div class="nuxbox-social col-md-6 col-sm-6 col-xs-12 no-paddingl">
                                        <a href="https://www.facebook.com/nuxorganicfoods" target="_blank"><i class="fa fa-facebook fa-3x"></i></a>
                                        <a href="https://www.instagram.com/nuxorganicfoods/" target="_blank"><i class="fa fa-instagram fa-3x"></i></a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <a href="<?php echo home_url('/product-category/singles/nut-mix/')?>"><button class="btn btn-nuxbox-hero">buy mix</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="partners-contact col-md-12 col-sm-12 col-xs-12">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?php $args = array('post_type' => 'partners', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date', 'meta_query' => array( array( 'key' => 'rw_partner-dest', 'value' => 1,),),); ?>
                        <?php query_posts($args); ?>
                        <?php while (have_posts()) : the_post(); ?>
                        <div class="col-md-5">
                            <a href="<?php echo get_post_meta(get_the_ID(), 'rw_partner-link', true); ?>" target="_blank"><?php the_title(); ?></a>
                        </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="nux-map col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <?php get_template_part('templates/map')?>
        </section>
    </div>
</main>
<?php
    get_footer();
