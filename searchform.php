<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
    <div class="input-group">
        <input type="text"  name="s" id="s" class="form-control" placeholder="Search for...">
        <span class="input-group-btn">
            <button id="searchsubmit" class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
        </span>
    </div><!-- /input-group -->
</form>