<?php get_header('shop'); ?>
<?php the_post(); ?>
<main class="container" role="main">
    <div class="row">
        <section class="nuxbox-creator-container col-md-12">


            <div class="nuxbox-creator-ads-container col-md-12 hidden-sm hidden-xs visible-md visible-lg">
                <a href="<?php echo home_url('/healthybox-creator'); ?>" title="See our Healthy Box">
                    <div class="col-md-9 col-md-offset-3">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/healthy-box.png" alt="" class="img-responsive img-ads" />
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/nux-prods.png" alt="" class="img-responsive img-ads" />
                        <a href="<?php echo home_url('/healthybox-creator'); ?>"><button class="btn btn-nuxbox-hero">buy box</button></a>
                    </div>
                </a>
            </div>
            <div class="nuxbox-creator-mobile-ads-container col-sm-12 visible-sm visible-xs hidden-md hidden-lg">
                <a href="<?php echo home_url('/healthybox-creator'); ?>" title="See our Healthy Box">

                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/healthybox-prods-mobile.png" alt="" class="img-responsive img-ads" />
                    <a href="<?php echo home_url('/healthybox-creator'); ?>"><button class="btn btn-nuxbox-hero">buy box</button></a>
                </a>
            </div>
            <div class="clearfix"></div>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h1 class="panel-title">
                            <a>
                                <span></span> <span class="nuxbox-creator-title">select <strong>the box</strong></span>
                            </a>
                        </h1>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <?php $args = array('post_type' => 'product', 'order'=> 'ASC', 'orderby' => 'date', 'posts_per_page' => 3, 'tax_query' => array( array( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => 'nux-box'))); ?>
                                <?php query_posts($args); $i = 1; ?>
                                <div class="col-md-10 col-md-offset-1">
                                    <?php while (have_posts()) : the_post(); ?>
                                    <div class="nuxbox-creator-item nuxbox-creator-item-<?php echo $i; ?> col-md-4 delay-1 animated">
                                        <h2><?php the_title(); ?></h2>
                                        <h4>4 <?php if ($i < 3 )  { echo 'spreads ('; } else { echo 'Nut Mixes'; } ?> <?php if ($i == 1 ) { echo '85ml'; } if ($i == 2 )  { echo '170ml'; } ?> <?php if ($i < 3 )  { echo ')'; } ?></h4>
                                        <div class="clearfix"></div>
                                        <a href="<?php the_permalink(); ?>">
                                            <div class="col-md-10">
                                                <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
                                            </div>
                                        </a>
                                    </div>
                                    <?php $i++; endwhile; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
