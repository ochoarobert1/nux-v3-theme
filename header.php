<!DOCTYPE html>
<html <?php language_attributes() ?>>
    <head>
        <meta charset="<?php bloginfo('charset') ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link href="//www.google-analytics.com" rel="dns-prefetch" />
        <link href="<?php echo get_template_directory_uri(); ?>/favicon.ico" rel="shortcut icon" />
        <meta name="robot" content="NOODP, INDEX, FOLLOW" />
        <meta name="title" content="nuxorganicfoods" />
        <meta name="language" content="VE" />
        <meta name="geo.position" content="49.611594, 6.131067" />
        <meta name="ICBM" content="49.611594, 6.131067" />
        <meta name="geo.region" content="LU" />
        <meta name="geo.placename" content="Luxembourg" />
        <meta name="DC.title" content="<?php wp_title('|', true, 'right'); ?>" />
        <title><?php wp_title('|', true, 'left'); ?></title>
        <meta name="title" content="<?php if(is_home()) { echo bloginfo("name"); echo " | "; echo bloginfo("description"); } else { echo wp_title(" | ", false, 'right'); echo bloginfo("name"); } ?>">
        <?php $current_url = $_SERVER['REQUEST_URI']; $clean_url = str_replace('/', '', $current_url); ?>
        <?php if(is_single()) : $the_slug = $clean_url; $args=array('name' => $the_slug, 'posts_per_page' => 1); $my_posts = get_posts( $args ); ?>
        <?php if( $my_posts ) { $excerpt = $my_posts[0]->post_excerpt; echo '<meta name="description" content="' . htmlentities($excerpt, ENT_QUOTES, 'UTF-8') . '" />'; } ?>
        <?php endif; ?>
        <?php if (is_home() ) { echo '<meta name="description" content="'. get_bloginfo("name") .' | ' . get_bloginfo("description") . '">'; } ?>
        <?php if (is_page() ) { echo '<meta name="description" content="'. get_bloginfo("name") .' | ' . get_bloginfo("description") . '">'; } ?>
        <?php if(is_single()) : $the_slug = $clean_url; $args=array('name' => $the_slug, 'posts_per_page' => 1); $my_posts = get_posts( $args ); ?>
        <?php if( $my_posts ) { $terms = get_the_terms( $my_posts[0]->ID, 'post_tag' ); if ( $terms && ! is_wp_error( $terms ) ) : $draught_links = array(); ?>
        <?php foreach ( $terms as $term ) { $draught_links[] = $term->name; } $on_draught = join( ", ", $draught_links ); endif;
                               echo '<meta name="keywords" content="nuxspreads , '. $on_draught .'" />'; } ?>
        <?php endif; ?>
        <?php if (is_home() || is_page() ) { echo '<meta name="keywords" content="nuxspreads" />'; } ?>
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@nuxorganicfoods " />
        <meta name="twitter:creator" content="@nuxorganicfoods " />
        <meta property="og:title" content="<?php wp_title('|', true, 'right'); ?>" />
        <meta property="og:site_name" content="nuxorganicfoods" />
        <meta property="og:type" content="article" />
        <meta property="og:locale" content="es_ES" />
        <meta property="og:url" content="<?php if(is_single()) { the_permalink(); } else { echo 'nuxspreads'; }?>" />
        <?php if(is_single()) : $the_slug = $clean_url; $args=array('name' => $the_slug, 'posts_per_page' => 1); $my_posts = get_posts( $args ); ?>
        <?php if( $my_posts ) { $excerpt = $my_posts[0]->post_excerpt; echo '<meta property="og:description" content="' . htmlentities($excerpt, ENT_QUOTES, 'UTF-8') . '" />'; } ?>
        <?php endif; ?>
        <?php if (is_home() ) { echo '<meta property="og:description" content="'. get_bloginfo("name") .'" | "' . get_bloginfo("description") . '">'; } ?>
        <?php if (is_page() ) { echo '<meta property="og:description"  content="'. get_bloginfo("name") .'" | "' . get_bloginfo("description") . '">'; } ?>
        <meta property="og:image" content='<?php if(is_single()){ $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); echo $url; } else { echo esc_url( get_template_directory_uri() ) ."/images/oglogo.png"; } ?>' />
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php wp_head() ?>
        <!--[if IE]> <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script> <![endif]-->
        <!--[if IE]>  <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script> <![endif]-->
        <link type="text/css" rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/nux-style.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/nux-mediaqueries.css" />

        <?php get_template_part('inc/ga-script'); ?>
        <script type="text/javascript">
            var ruta_blog = "<?php echo esc_url(get_template_directory_uri()); ?>";
        </script>
    </head>
    <body <?php body_class() ?>>
        <div id="fb-root"></div>
        <header class="container-fluid">
            <div class="row">
                <div id="sticker" class="the-header col-md-12 no-paddingl no-paddingr">
                    <div class="container hidden-xs">
                        <div class="row">
                            <div class="header-logo-container col-md-12">
                                <a href="<?php echo home_url('/'); ?>"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="Nux Nut Spreads" title="Nux - Nut Spreads" class="img-responsive" /></a>
                            </div>
                            <div class="header-social-header col-md-3 col-sm-3 col-xs-3">
                                <a href="https://www.facebook.com/nuxorganicfoods" target="_blank"><i class="fa fa-facebook fa-3x"></i></a>
                                <a href="https://www.instagram.com/nuxorganicfoods/" target="_blank"><i class="fa fa-instagram fa-3x"></i></a>
                            </div>
                            <div class="header-menu-container col-md-6 col-sm-6 col-xs-6">
                                <nav class="navbar navbar-default">
                                    <div class="container-fluid">
                                        <!-- Brand and toggle get grouped for better mobile display -->
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>

                                        <!-- Collect the nav links, forms, and other content for toggling -->
                                        <?php wp_nav_menu( array( 'theme_location' => 'header_menu', 'depth' => 2, 'container' => 'div',
                                                                 'container_class' => 'collapse navbar-collapse', 'container_id' => 'bs-example-navbar-collapse-1', 'menu_class' => 'nav navbar-nav', 'fallback_cb' => 'wp_bootstrap_navwalker::fallback', 'walker' => new wp_bootstrap_navwalker() )); ?>
                                    </div><!-- /.container-fluid -->
                                </nav>
                            </div>
                            <div class="header-extra-container col-md-3 col-sm-3 col-xs-3">
                                <a href="<?php echo home_url('/cart') ?>">
                                    <i class="fa fa-shopping-cart">
                                        <?php global $woocommerce; $count = $woocommerce->cart->cart_contents_count; ?>
                                        <?php if ($count > 0) { ?>
                                        <span class="badge"><?php echo $count; ?></span>
                                        <?php } ?>
                                    </i>
                                </a>
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/lux.png" alt="Made in Luxenbourgh" class="img-responsive" />
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/bio.png" alt="Bio" class="img-responsive" />
                            </div>
                        </div>
                    </div>
                    <div class="container hidden-lg hidden-md hidden-sm">
                        <div class="row">
                            <div class="header-mini-social-container col-xs-12">
                                <div class="col-xs-6">
                                    <a href="https://www.facebook.com/nuxorganicfoods" target="_blank"><i class="fa fa-facebook"></i></a>
                                    <a href="https://www.instagram.com/nuxorganicfoods/" target="_blank"><i class="fa fa-instagram"></i></a>
                                </div>
                                <div class="header-mini-cart-container col-xs-6 no-paddingr">
                                <a href="<?php echo home_url('/cart') ?>">
                                    <i class="fa fa-shopping-cart">
                                        <?php global $woocommerce; $count = $woocommerce->cart->cart_contents_count; ?>
                                        <?php if ($count > 0) { ?>
                                        <span class="badge"><?php echo $count; ?></span>
                                        <?php } ?>
                                    </i>
                                </a>
                                </div>
                            </div>

                            <div class="header-mini-menu-container col-xs-12">
                                <nav class="navbar navbar-default">
                                    <div class="container-fluid">
                                        <!-- Brand and toggle get grouped for better mobile display -->
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                            <a href="<?php echo home_url('/'); ?>" class="navbar-brand"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="Nux Nut Spreads" title="Nux - Nut Spreads" class="img-responsive" /></a>
                                        </div>

                                        <!-- Collect the nav links, forms, and other content for toggling -->
                                        <?php wp_nav_menu( array( 'theme_location' => 'header_menu', 'depth' => 2, 'container' => 'div',
                                                                 'container_class' => 'collapse navbar-collapse', 'container_id' => 'bs-example-navbar-collapse-2', 'menu_class' => 'nav navbar-nav', 'fallback_cb' => 'wp_bootstrap_navwalker::fallback', 'walker' => new wp_bootstrap_navwalker() )); ?>
                                    </div><!-- /.container-fluid -->
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
