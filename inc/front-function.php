<?php
if ($_SERVER['REMOTE_ADDR'] == '::1') {
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp_nux/wp-load.php' );
} else {
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );
}
global $wpdb;

$datos = $_POST['datos'];
$thepost = get_post($datos);

$image = rwmb_meta( 'rw_title_img', 'type=image&size=full', $datos ); foreach ($image as $img) { $url = $img['full_url']; } ?>
<img src="<?php echo $url; ?>" alt="<?php echo $thepost->post_title; ?>" class="front-product-title img-responsive animated fadeIn" />
<div class="front-product-single-img col-md-12 animated fadeIn">
    <?php echo get_the_post_thumbnail( $datos, 'full' ); ?>
</div>
<a href="<?php echo get_post_permalink($datos); ?>"><button class="front-product-single-btn animated fadeIn">buy</button></a>
