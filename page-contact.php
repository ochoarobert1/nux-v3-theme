<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main">
    <div class="row">
        <section class="contact-container col-md-12">
            <div class="contact-container-mask"></div>
            <div class="container">
                <div class="row">
                    <div class="contact-content col-md-12">
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <div class="col-md-12">
                                <?php the_content(); ?>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>friends &amp; partners</h2>
                    </div>
                </div>
            </div>
            <div class="partners-contact col-md-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                           <?php $args = array('post_type' => 'partners', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date', 'meta_query' => array( array( 'key' => 'rw_partner-dest', 'value' => 1,),),); ?>
                           <?php query_posts($args); ?>
                           <?php while (have_posts()) : the_post(); ?>
                           <div class="col-md-5">
                               <a href="<?php echo get_post_meta(get_the_ID(), 'rw_partner-link', true); ?>" target="_blank"><?php the_title(); ?></a>
                           </div>
                           <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="nux-map col-md-12 no-paddingl no-paddingr">
                <?php get_template_part('templates/map')?>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
